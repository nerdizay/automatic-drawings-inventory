import FileReaderComponent from "./components/FileReaderComponent"
import InventoryFile from "./components/InventoryFile"

import { useState } from "react"

export default function App() {

  let [inventories, setInventories] = useState([
    {
      filename: "Пример файла.pdf",
      formatsAndPages: [
        {
          format: "A4",
          count: 15,
          pages: "1-15"
        }
      ]
    },
    {
      filename: "Пример файла 2.pdf",
      formatsAndPages: [
        {
          format: "A3",
          count: 15,
          pages: "1-15"
        }
      ]
    }
  ])

  const refreshInventories = (inventory) => setInventories(inventory)


  return (
    <>
      <FileReaderComponent refreshInventories={refreshInventories}/>
      {inventories.map(inventory => {
          return <InventoryFile
            key={inventory.filename}
            filename={inventory.filename}
            formatsAndPages={inventory.formatsAndPages}
          />
        }
      )}
    </>
  )
}

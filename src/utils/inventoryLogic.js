class Format{
    constructor(name, width, length){
        this.name = name
        this.width = width
        this.length = length
    }
    static nextFormat(name){
        if (name === "A4"){ return "A3" }
        else if (name === "A3"){ return "A2" }
        else if (name === "A2"){ return "A1" }
        else if (name === "A1"){ return "A0" }
        else{ return null }
    }
}

const A4 = new Format("A4", 297, 210)
const A3 = new Format("A3", 420, 297)
const A2 = new Format("A2", 594, 420)
const A1 = new Format("A1", 841, 594)
const A0 = new Format("A0", 1189, 841)

const baseFormats = [A4, A3, A2, A1, A0]

const beautifulName = (name) => {
    // Если не равно -1 значит присутствует в строке
    if (name.indexOf("x1") !== -1){
        // Доп проверка, потому что может быть x1 и x11
        if (!name.split("x1")[1]){
            return name.split("x1")[0]
        }
        else{
            return name
        }
    }
    else if (name.indexOf("x2") !== -1){
        let baseName = name.split("x2")[0]
        let nextFormat = Format.nextFormat(baseName)
        if (nextFormat){
            return nextFormat
        }
    }
    return name
}

const getAllLengthFormat = (baseLength, baseName) => {
    const res = {}
    for (
        let length = baseLength, multiplicity = 1;
        length < baseLength*15;
        length += baseLength, multiplicity++
    ){
        res[length] = beautifulName(baseName+"x"+multiplicity)
    }
    return res
}

const getAllFormatsTree = () => {
    const res = {}
    for (let format of baseFormats){
        res[format.width] = getAllLengthFormat(format.length, format.name)
    }
    return res
}

const allFormatsTree = getAllFormatsTree()


function getClearMMString(size){
    if (typeof size === "number"){
        size = String(Math.trunc(size))
        return size
    }
    else if (typeof size === "string"){
        size = Number(size)
        size = String(Math.trunc(size))
        return size
    }
    return null
}

function defineFormat(width, length, alreadyReverse=false, allowance=5){
// Ожидает длины в милиметрах
// alreadyReverse - для прерывания рекурсии, при вызове функции не передавать этот аргумент
// allowance - погрешность в миллиметрах
    
    width = getClearMMString(width)
    length = getClearMMString(length)

    for (
        let widthAllowance = 0 - allowance;
        widthAllowance <= allowance;
        widthAllowance++
    ){
        for (
            let lengthAllowance = 0 - allowance;
            lengthAllowance <= allowance;
            lengthAllowance++
        ){
            let baseFormat = allFormatsTree[Number(width)+widthAllowance]
            if (baseFormat === undefined){
                break
            }
            let concreteFormat = baseFormat[Number(length)+lengthAllowance]
            if (concreteFormat === undefined){
                continue
            }
            return concreteFormat
        }
    }
    if (alreadyReverse === true){
        return `${width}x${length}`
    }
    else{
        return defineFormat(length, width, alreadyReverse=true)
    }
    
}

function ShortRecordPages(pages){
    let res = ""
    let previous = null
    pages.forEach(page => {
        if (!previous) {
            res += String(page)
        }
        else if (page - previous === 1){
            res = res + "-" + String(page)
        }
        else {
            res = res + "," + String(page)
        }
        
        previous = page
        
    })
    res = res.replace(/-(\d|-){1,}-/g, "-")
    
    return res
}

// Расчитано на использование библиотеки pdf-lib.min.js
// pdf = await PDFLib.PDFDocument.load(reader.result)
function getPages(pdf){
    const pages = []
    const pageCount = pdf.getPageCount()
    for (let pageNumber = 0; pageNumber < pageCount; pageNumber++){
      pages.push(pdf.getPage(pageNumber))
    }
    return pages
}
  
// Расчитано на использование библиотеки pdf-lib.min.js
// pdf = await PDFLib.PDFDocument.load(reader.result)
function getSize(page){
    let width = page.getWidth()
    let height = page.getHeight()
    width = Math.round(width*0.3528)
    length = Math.round(height*0.3528)
    return [width, length]
}

function analysePDF(pdf, getPages, getSize, getName){
    const formatsInPDF = []
    const res = {}
    const pages = getPages(pdf)
    res.filename = getName(pdf)
    res.formatsAndPages = []

    let pageNumber = 1;
    pages.forEach(page => {
        let [width, length] = getSize(page)
        let format = defineFormat(width, length)

        if (!formatsInPDF.includes(format)){
            formatsInPDF.push(format)
        }

        const formatsAndPages = (res.formatsAndPages.filter(item => {
            return item.format === format
        }))[0]

        if (!formatsAndPages){
            res.formatsAndPages.push({
                format: format,
                count: 1,
                pages: [pageNumber]
            })
        }
        else{
            formatsAndPages.count += 1
            formatsAndPages.pages.push(pageNumber)
        }

        pageNumber++
    })

    for (let formatAndPages of res.formatsAndPages){
        formatAndPages.pages = ShortRecordPages(formatAndPages.pages)
    }

    return res
}

export {
    analysePDF,
    getSize,
    getPages
};
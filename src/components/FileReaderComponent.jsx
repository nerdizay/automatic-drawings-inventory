import { analysePDF, getPages, getSize }  from '../utils/inventoryLogic.js'


function createInventories(refreshInventories){

    const logic = (event) => {

        const files = Array.from(event.target.files)
        if (files.length === 0) return
    
        Promise.all(
    
            files.map(file => {
    
                return new Promise((resolve, reject)=>{
                    const errorMessage = `Не удалось прочитать файл ${file.name}`
                    const reader = new FileReader()
    
                    reader.onload = event => {
                        PDFLib.PDFDocument
                        .load(event.target.result)
                        .then(PDF => {
                            resolve({PDF, filename: file.name})
                        })
                    }
                    reader.onabort = event => {reject(errorMessage)}
                    reader.onerror = event => {reject(errorMessage)}
                    try{
                        reader.readAsArrayBuffer(file)
                    }
                    catch (error){
                        reject(errorMessage)
                    }
                })
            })
    
        ).then(PDFsInfo => {
            const inventories = []
            PDFsInfo.forEach( PDFInfo =>{
                const {PDF, filename} = PDFInfo
                inventories.push(
                    analysePDF(PDF, getPages, getSize, (PDF)=> filename)
                )
            })
            refreshInventories(inventories)
    
        }).catch(errorMessage => {
            console.error(errorMessage)
        })
    }

    return logic
}

export default function FileReaderComponent({refreshInventories}){

    const style = {
        fontSize: 16,
        height: 50
    }
    return (
        <>
            <label
                htmlFor="filereader-input"
                id="filereader"
                style={{
                    backgroundColor: "rgb(217, 221, 220)",
                    maxWidth: "400px",
                    height: `${style.height}px`,
                    fontSize: `${style.fontSize}px`,
                    borderRadius: "5px",
                    display: "block",
                    textAlign: "center",
                    paddingTop: `${(style.height - style.fontSize)/2}px`,
                    paddingBottom: `${(style.height - style.fontSize)/2}px`,
                    borderBottom: "2px solid rgb(200, 203, 203)",
                    borderRight: "2px solid rgb(200, 203, 203)"
                }}
            >
                Выберите файлы
                <input
                    id="filereader-input"
                    type="file"
                    multiple
                    accept="application/pdf"
                    style={{display: "none"}}
                    onChange={createInventories(refreshInventories)}
                ></input>
            </label>
        </>
    )
}
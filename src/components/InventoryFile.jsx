export default function InventoryFile({filename, formatsAndPages}){
    return (
        <>
            <h3>{filename}</h3>
            {formatsAndPages.map(data => {
                return  (
                    <div key={data.format}>
                        <span>{data.format}</span>
                        <span style={{margin: "0px 3px"}}>({data.count})</span>
                        <span>{data.pages}</span>
                    </div>
                )}
            )}
            
        </>
    )
}